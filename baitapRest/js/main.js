
let tinhDTBKhoi2 = (van, su, dia, engLish) => {
    van = document.getElementById("inpVan").value * 1;
    su = document.getElementById("inpSu").value * 1;
    dia = document.getElementById("inpDia").value * 1;
    engLish = document.getElementById("inpEnglish").value * 1;

    dtbKhoiLopHai = (van + su + dia + engLish) /4;
    document.getElementById("tbKhoi2").innerHTML = dtbKhoiLopHai.toFixed(2);
};


let tinhDTBKhoi1 = (toan, ly, hoa, ...thongTinThem) => {
    toan = document.getElementById("inpToan").value * 1;
    ly = document.getElementById("inpLy").value * 1;
    hoa = document.getElementById("inpHoa").value * 1;
    tinhDTBKhoi2(thongTinThem);

    dtbKhoiLopMot = (toan + ly + hoa) / 3;
    document.getElementById("tbKhoi1").innerHTML = dtbKhoiLopMot.toFixed(2);
};